EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logo
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:Power_Management
LIBS:powerint
LIBS:maxim
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:_electromech
LIBS:_linear
LIBS:_logic
LIBS:_passive
LIBS:_semi
LIBS:74hct125d
LIBS:74hct245
LIBS:75176
LIBS:Abracon
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:AMS
LIBS:AnalogDevices
LIBS:AOS
LIBS:arm-swd-header
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:DiodesInc
LIBS:electomech-misc
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:Fairchild
LIBS:hm-11
LIBS:iso15
LIBS:LinearTech
LIBS:Littelfuse
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:lpc11u14fbd48
LIBS:MACOM
LIBS:Macrofab
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micro_usb_socket
LIBS:Micron
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nRF24L01+
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:OceanOptics
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:pp_ws2812b
LIBS:Recom
LIBS:recom-r1se
LIBS:Richtek
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:skyworks
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:symbol
LIBS:TexasInstruments
LIBS:uart_pp
LIBS:u-blox
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:Control_Board_Rev_A-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR048
U 1 1 59B4AF55
P 5375 2750
F 0 "#PWR048" H 5375 2500 50  0001 C CNN
F 1 "GND" H 5375 2600 50  0000 C CNN
F 2 "" H 5375 2750 50  0001 C CNN
F 3 "" H 5375 2750 50  0001 C CNN
	1    5375 2750
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 59B4AF5E
P 6075 3975
F 0 "R13" V 6155 3975 50  0000 C CNN
F 1 "2.2k" V 6075 3975 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6005 3975 50  0001 C CNN
F 3 "" H 6075 3975 50  0001 C CNN
	1    6075 3975
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 59B4AF65
P 6300 3975
F 0 "R14" V 6380 3975 50  0000 C CNN
F 1 "3.3k" V 6300 3975 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6230 3975 50  0001 C CNN
F 3 "" H 6300 3975 50  0001 C CNN
	1    6300 3975
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR049
U 1 1 59B4AF6E
P 5425 4625
F 0 "#PWR049" H 5425 4375 50  0001 C CNN
F 1 "GND" H 5425 4475 50  0000 C CNN
F 2 "" H 5425 4625 50  0001 C CNN
F 3 "" H 5425 4625 50  0001 C CNN
	1    5425 4625
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 59B4B5B0
P 5325 3050
F 0 "#PWR050" H 5325 2800 50  0001 C CNN
F 1 "GND" H 5325 2900 50  0000 C CNN
F 2 "" H 5325 3050 50  0001 C CNN
F 3 "" H 5325 3050 50  0001 C CNN
	1    5325 3050
	0    -1   -1   0   
$EndComp
Text HLabel 6475 2550 2    60   Output ~ 0
TSense
Text HLabel 5225 3150 2    60   Output ~ 0
MSense
Text HLabel 5250 3675 2    60   Input ~ 0
GPS_RX
Text HLabel 5225 3775 2    60   Output ~ 0
GPS_TX
Text HLabel 5250 3875 2    60   Input ~ 0
GPS_EN
Text HLabel 6500 4625 2    60   BiDi ~ 0
SDA
Text HLabel 6500 4800 2    60   Input ~ 0
SCL
Text HLabel 5225 4500 2    60   Input ~ 0
RST_IMU
$Comp
L CONN_01X05 J11
U 1 1 59B52BC3
P 5000 3775
F 0 "J11" H 5000 4075 50  0000 C CNN
F 1 "CONN_01X05" V 5100 3775 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 3775 50  0001 C CNN
F 3 "https://www.adafruit.com/product/746" H 5000 3775 50  0001 C CNN
	1    5000 3775
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 59B5334B
P 5425 4000
F 0 "#PWR051" H 5425 3750 50  0001 C CNN
F 1 "GND" H 5425 3850 50  0000 C CNN
F 2 "" H 5425 4000 50  0001 C CNN
F 3 "" H 5425 4000 50  0001 C CNN
	1    5425 4000
	1    0    0    -1  
$EndComp
Text Notes 4750 2625 2    60   ~ 0
Temp Sense\n
Text Notes 4775 3150 2    60   ~ 0
Moisture Sense\n
Text Notes 4625 3850 2    60   ~ 0
GPS\n
Text Notes 4625 4475 2    60   ~ 0
IMU\n
$Comp
L GND #PWR052
U 1 1 59B81F82
P 5425 5250
F 0 "#PWR052" H 5425 5000 50  0001 C CNN
F 1 "GND" H 5425 5100 50  0000 C CNN
F 2 "" H 5425 5250 50  0001 C CNN
F 3 "" H 5425 5250 50  0001 C CNN
	1    5425 5250
	1    0    0    -1  
$EndComp
Text HLabel 5200 5125 2    60   Input ~ 0
RST_IMU_2
Text Notes 4175 4900 2    60   ~ 0
Connect 3vo to ADR pin on IMU\n (test to see if this works). \nThis will change the address \nof imu from 0x28 to 0x29.\n
Text Notes 4650 5075 2    60   ~ 0
IMU_2
$Comp
L CONN_01X05 J12
U 1 1 59B867A5
P 5000 4400
F 0 "J12" H 5000 4700 50  0000 C CNN
F 1 "CONN_01X05" V 5100 4400 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 4400 50  0001 C CNN
F 3 "https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview" H 5000 4400 50  0001 C CNN
	1    5000 4400
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X05 J13
U 1 1 59B86CA3
P 5000 5025
F 0 "J13" H 5000 5325 50  0000 C CNN
F 1 "CONN_01X05" V 5100 5025 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 5025 50  0001 C CNN
F 3 "https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview" H 5000 5025 50  0001 C CNN
	1    5000 5025
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR053
U 1 1 59B87D04
P 5425 5900
F 0 "#PWR053" H 5425 5650 50  0001 C CNN
F 1 "GND" H 5425 5750 50  0000 C CNN
F 2 "" H 5425 5900 50  0001 C CNN
F 3 "" H 5425 5900 50  0001 C CNN
	1    5425 5900
	1    0    0    -1  
$EndComp
Text Notes 4700 5625 2    60   ~ 0
Altimeter
Text Label 5225 4300 0    60   ~ 0
SDA
Text Label 5225 4400 0    60   ~ 0
SCL
Text Label 5225 4925 0    60   ~ 0
SDA
Text Label 5225 5025 0    60   ~ 0
SCL
Text Label 5225 5550 0    60   ~ 0
SDA
Text Label 5225 5650 0    60   ~ 0
SCL
$Comp
L +3.3V #PWR054
U 1 1 59B8D566
P 5750 2050
F 0 "#PWR054" H 5750 1900 50  0001 C CNN
F 1 "+3.3V" H 5750 2190 50  0000 C CNN
F 2 "" H 5750 2050 50  0001 C CNN
F 3 "" H 5750 2050 50  0001 C CNN
	1    5750 2050
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 59C7E60C
P 6225 2350
F 0 "R15" V 6305 2350 50  0000 C CNN
F 1 "4.7k" V 6225 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6155 2350 50  0001 C CNN
F 3 "" H 6225 2350 50  0001 C CNN
	1    6225 2350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J8
U 1 1 59D55B5D
P 5000 2550
F 0 "J8" H 5000 2850 50  0000 C CNN
F 1 "CONN_01X05" V 5100 2550 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 2550 50  0001 C CNN
F 3 "" H 5000 2550 50  0001 C CNN
	1    5000 2550
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X05 J10
U 1 1 59D55FCB
P 5000 3150
F 0 "J10" H 5000 3450 50  0000 C CNN
F 1 "CONN_01X05" V 5100 3150 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 3150 50  0001 C CNN
F 3 "" H 5000 3150 50  0001 C CNN
	1    5000 3150
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR055
U 1 1 59D56571
P 5300 3375
F 0 "#PWR055" H 5300 3125 50  0001 C CNN
F 1 "GND" H 5300 3225 50  0000 C CNN
F 2 "" H 5300 3375 50  0001 C CNN
F 3 "" H 5300 3375 50  0001 C CNN
	1    5300 3375
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J14
U 1 1 59D577A3
P 5000 5650
F 0 "J14" H 5000 5950 50  0000 C CNN
F 1 "CONN_01X05" V 5100 5650 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 5000 5650 50  0001 C CNN
F 3 "https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview" H 5000 5650 50  0001 C CNN
	1    5000 5650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5200 2550 5375 2550
Wire Wire Line
	5200 3050 5325 3050
Wire Wire Line
	5200 3675 5250 3675
Wire Wire Line
	5200 3775 5225 3775
Wire Wire Line
	5200 3875 5250 3875
Wire Wire Line
	5200 4600 5425 4600
Wire Wire Line
	5425 4600 5425 4625
Wire Wire Line
	5200 4500 5225 4500
Wire Wire Line
	5750 2950 5200 2950
Connection ~ 5750 2950
Connection ~ 5750 3575
Wire Wire Line
	5200 3150 5225 3150
Wire Wire Line
	5200 3975 5425 3975
Wire Wire Line
	5425 3975 5425 4000
Wire Wire Line
	5200 5225 5425 5225
Wire Wire Line
	5425 5225 5425 5250
Wire Wire Line
	5200 5125 5200 5125
Wire Wire Line
	5750 4200 5200 4200
Wire Wire Line
	5200 3575 6300 3575
Wire Wire Line
	5200 4300 6075 4300
Wire Wire Line
	5200 4400 6300 4400
Wire Wire Line
	6300 5025 5200 5025
Wire Wire Line
	6075 4925 5200 4925
Wire Wire Line
	5750 4825 5200 4825
Connection ~ 5750 4200
Wire Wire Line
	5750 5450 5200 5450
Connection ~ 5750 4825
Wire Wire Line
	6075 5550 5200 5550
Wire Wire Line
	6300 5650 5200 5650
Wire Wire Line
	5200 5750 5425 5750
Wire Wire Line
	5425 5750 5425 5900
Wire Wire Line
	6075 4125 6075 5550
Wire Wire Line
	6075 3575 6075 3825
Wire Wire Line
	6300 3575 6300 3825
Connection ~ 6075 3575
Wire Wire Line
	6300 4125 6300 5650
Connection ~ 6075 4300
Connection ~ 6300 4400
Connection ~ 6075 4925
Connection ~ 6300 5025
Wire Wire Line
	6075 4625 6500 4625
Connection ~ 6075 4625
Wire Wire Line
	6300 4800 6500 4800
Connection ~ 6300 4800
Wire Wire Line
	6075 2550 6475 2550
Connection ~ 6225 2550
Wire Wire Line
	5200 3250 5300 3250
Wire Wire Line
	5300 3250 5300 3375
Wire Wire Line
	5300 3350 5200 3350
Connection ~ 5300 3350
Wire Wire Line
	5200 2450 6075 2450
Wire Wire Line
	6075 2450 6075 2550
Wire Wire Line
	6225 2200 6075 2200
Wire Wire Line
	6075 2200 6075 2350
Connection ~ 5750 2350
Wire Wire Line
	6075 2350 5200 2350
Wire Wire Line
	6225 2500 6225 2550
Wire Wire Line
	5750 2050 5750 5450
Wire Wire Line
	5200 5850 5425 5850
Connection ~ 5425 5850
Wire Wire Line
	5375 2550 5375 2750
Wire Wire Line
	5200 2650 5375 2650
Connection ~ 5375 2650
Wire Wire Line
	5375 2750 5200 2750
Connection ~ 5375 2750
$EndSCHEMATC
