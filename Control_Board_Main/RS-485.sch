EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logo
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:Power_Management
LIBS:powerint
LIBS:maxim
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:_electromech
LIBS:_linear
LIBS:_logic
LIBS:_passive
LIBS:_semi
LIBS:74hct125d
LIBS:74hct245
LIBS:75176
LIBS:Abracon
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:AMS
LIBS:AnalogDevices
LIBS:AOS
LIBS:arm-swd-header
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:DiodesInc
LIBS:electomech-misc
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:Fairchild
LIBS:hm-11
LIBS:iso15
LIBS:LinearTech
LIBS:Littelfuse
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:lpc11u14fbd48
LIBS:MACOM
LIBS:Macrofab
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micro_usb_socket
LIBS:Micron
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nRF24L01+
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:OceanOptics
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:pp_ws2812b
LIBS:Recom
LIBS:recom-r1se
LIBS:Richtek
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:skyworks
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:symbol
LIBS:TexasInstruments
LIBS:uart_pp
LIBS:u-blox
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:Control_Board_Rev_A-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 75176 U3
U 1 1 595CEB9D
P 3050 1250
F 0 "U3" H 3050 1600 60  0000 C CNN
F 1 "75176" H 3050 1500 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 950 60  0001 C CNN
F 3 "" H 3000 1650 60  0000 C CNN
	1    3050 1250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 595CFAFE
P 2450 1550
F 0 "#PWR036" H 2450 1300 50  0001 C CNN
F 1 "GND" H 2450 1400 50  0000 C CNN
F 2 "" H 2450 1550 50  0001 C CNN
F 3 "" H 2450 1550 50  0001 C CNN
	1    2450 1550
	1    0    0    -1  
$EndComp
Text HLabel 2150 1200 0    60   BiDi ~ 0
B
Text HLabel 2150 1300 0    60   BiDi ~ 0
A
$Comp
L 75176 U4
U 1 1 595CFCFD
P 3050 2400
F 0 "U4" H 3050 2750 60  0000 C CNN
F 1 "75176" H 3050 2650 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 2100 60  0001 C CNN
F 3 "" H 3000 2800 60  0000 C CNN
	1    3050 2400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 595CFD0F
P 2450 2700
F 0 "#PWR037" H 2450 2450 50  0001 C CNN
F 1 "GND" H 2450 2550 50  0000 C CNN
F 2 "" H 2450 2700 50  0001 C CNN
F 3 "" H 2450 2700 50  0001 C CNN
	1    2450 2700
	1    0    0    -1  
$EndComp
Text HLabel 2150 2350 0    60   BiDi ~ 0
B
Text HLabel 2150 2450 0    60   BiDi ~ 0
A
$Comp
L 75176 U5
U 1 1 595D00CB
P 3050 3550
F 0 "U5" H 3050 3900 60  0000 C CNN
F 1 "75176" H 3050 3800 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 3250 60  0001 C CNN
F 3 "" H 3000 3950 60  0000 C CNN
	1    3050 3550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR038
U 1 1 595D00DD
P 2450 3850
F 0 "#PWR038" H 2450 3600 50  0001 C CNN
F 1 "GND" H 2450 3700 50  0000 C CNN
F 2 "" H 2450 3850 50  0001 C CNN
F 3 "" H 2450 3850 50  0001 C CNN
	1    2450 3850
	1    0    0    -1  
$EndComp
Text HLabel 2150 3500 0    60   BiDi ~ 0
B
Text HLabel 2150 3600 0    60   BiDi ~ 0
A
$Comp
L 75176 U6
U 1 1 595D00ED
P 3050 4700
F 0 "U6" H 3050 5050 60  0000 C CNN
F 1 "75176" H 3050 4950 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 4400 60  0001 C CNN
F 3 "" H 3000 5100 60  0000 C CNN
	1    3050 4700
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR039
U 1 1 595D00FF
P 2450 5000
F 0 "#PWR039" H 2450 4750 50  0001 C CNN
F 1 "GND" H 2450 4850 50  0000 C CNN
F 2 "" H 2450 5000 50  0001 C CNN
F 3 "" H 2450 5000 50  0001 C CNN
	1    2450 5000
	1    0    0    -1  
$EndComp
Text HLabel 2150 4650 0    60   BiDi ~ 0
B
Text HLabel 2150 4750 0    60   BiDi ~ 0
A
$Comp
L 75176 U7
U 1 1 595D0386
P 3050 5850
F 0 "U7" H 3050 6200 60  0000 C CNN
F 1 "75176" H 3050 6100 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 5550 60  0001 C CNN
F 3 "" H 3000 6250 60  0000 C CNN
	1    3050 5850
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR040
U 1 1 595D0398
P 2450 6150
F 0 "#PWR040" H 2450 5900 50  0001 C CNN
F 1 "GND" H 2450 6000 50  0000 C CNN
F 2 "" H 2450 6150 50  0001 C CNN
F 3 "" H 2450 6150 50  0001 C CNN
	1    2450 6150
	1    0    0    -1  
$EndComp
Text HLabel 2150 5800 0    60   BiDi ~ 0
B
Text HLabel 2150 5900 0    60   BiDi ~ 0
A
$Comp
L 75176 U8
U 1 1 595D03A8
P 3050 7000
F 0 "U8" H 3050 7350 60  0000 C CNN
F 1 "75176" H 3050 7250 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3050 6700 60  0001 C CNN
F 3 "" H 3000 7400 60  0000 C CNN
	1    3050 7000
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR041
U 1 1 595D03BA
P 2450 7300
F 0 "#PWR041" H 2450 7050 50  0001 C CNN
F 1 "GND" H 2450 7150 50  0000 C CNN
F 2 "" H 2450 7300 50  0001 C CNN
F 3 "" H 2450 7300 50  0001 C CNN
	1    2450 7300
	1    0    0    -1  
$EndComp
Text HLabel 2150 6950 0    60   BiDi ~ 0
B
Text HLabel 2150 7050 0    60   BiDi ~ 0
A
$Comp
L R R4
U 1 1 595FE21B
P 1650 1250
F 0 "R4" V 1730 1250 50  0000 C CNN
F 1 "120" V 1650 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1580 1250 50  0001 C CNN
F 3 "" H 1650 1250 50  0001 C CNN
	1    1650 1250
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 59678297
P 1650 7000
F 0 "R5" V 1730 7000 50  0000 C CNN
F 1 "120" V 1650 7000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1580 7000 50  0001 C CNN
F 3 "" H 1650 7000 50  0001 C CNN
	1    1650 7000
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 597A74FC
P 1325 1250
F 0 "C9" H 1350 1350 50  0000 L CNN
F 1 ".1uF" H 1350 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1363 1100 50  0001 C CNN
F 3 "" H 1325 1250 50  0001 C CNN
	1    1325 1250
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 597A82A8
P 1775 2350
F 0 "C14" H 1800 2450 50  0000 L CNN
F 1 ".1uF" H 1800 2250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1813 2200 50  0001 C CNN
F 3 "" H 1775 2350 50  0001 C CNN
	1    1775 2350
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 597A8524
P 1775 3525
F 0 "C13" H 1800 3625 50  0000 L CNN
F 1 ".1uF" H 1800 3425 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1813 3375 50  0001 C CNN
F 3 "" H 1775 3525 50  0001 C CNN
	1    1775 3525
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 597A87E5
P 1750 4675
F 0 "C11" H 1775 4775 50  0000 L CNN
F 1 ".1uF" H 1775 4575 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1788 4525 50  0001 C CNN
F 3 "" H 1750 4675 50  0001 C CNN
	1    1750 4675
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 597A8BE4
P 1750 5825
F 0 "C12" H 1775 5925 50  0000 L CNN
F 1 ".1uF" H 1775 5725 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1788 5675 50  0001 C CNN
F 3 "" H 1750 5825 50  0001 C CNN
	1    1750 5825
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 597AA6DF
P 1375 6975
F 0 "C10" H 1400 7075 50  0000 L CNN
F 1 ".1uF" H 1400 6875 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1413 6825 50  0001 C CNN
F 3 "" H 1375 6975 50  0001 C CNN
	1    1375 6975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1100 2600 1100
Wire Wire Line
	2450 950  2450 1100
Wire Wire Line
	2150 1200 2600 1200
Wire Wire Line
	2150 1300 2600 1300
Wire Wire Line
	2450 1400 2600 1400
Wire Wire Line
	2450 1400 2450 1550
Wire Wire Line
	3500 1100 3925 1100
Wire Wire Line
	3500 1200 3500 1300
Wire Wire Line
	3500 1250 4050 1250
Connection ~ 3500 1250
Wire Wire Line
	3500 1400 4200 1400
Wire Wire Line
	2450 2250 2600 2250
Wire Wire Line
	2450 2100 2450 2250
Wire Wire Line
	2600 2350 2150 2350
Wire Wire Line
	2600 2450 2150 2450
Wire Wire Line
	1850 2550 2600 2550
Wire Wire Line
	2450 2550 2450 2700
Wire Wire Line
	3500 2250 3950 2250
Wire Wire Line
	3500 2350 3500 2450
Wire Wire Line
	3500 2400 4100 2400
Connection ~ 3500 2400
Wire Wire Line
	3500 2550 4250 2550
Wire Wire Line
	2450 3400 2600 3400
Wire Wire Line
	2450 3250 2450 3400
Wire Wire Line
	2600 3500 2150 3500
Wire Wire Line
	2600 3600 2150 3600
Wire Wire Line
	2450 3700 2600 3700
Wire Wire Line
	2450 3700 2450 3850
Wire Wire Line
	3500 3400 3950 3400
Wire Wire Line
	3500 3500 3500 3600
Connection ~ 3500 3550
Wire Wire Line
	3500 3700 4250 3700
Wire Wire Line
	2450 4550 2600 4550
Wire Wire Line
	2450 4400 2450 4550
Wire Wire Line
	2600 4650 2150 4650
Wire Wire Line
	2600 4750 2150 4750
Wire Wire Line
	2450 4850 2600 4850
Wire Wire Line
	2450 4850 2450 5000
Wire Wire Line
	3500 4550 3950 4550
Wire Wire Line
	3500 4650 3500 4750
Wire Wire Line
	3500 4700 4100 4700
Connection ~ 3500 4700
Wire Wire Line
	3500 4850 4250 4850
Wire Wire Line
	2450 5700 2600 5700
Wire Wire Line
	2450 5550 2450 5700
Wire Wire Line
	2600 5800 2150 5800
Wire Wire Line
	2600 5900 2150 5900
Wire Wire Line
	2450 6000 2600 6000
Wire Wire Line
	2450 6000 2450 6150
Wire Wire Line
	3500 5700 3950 5700
Wire Wire Line
	3500 5800 3500 5900
Wire Wire Line
	3500 5850 4075 5850
Connection ~ 3500 5850
Wire Wire Line
	3500 6000 4200 6000
Wire Wire Line
	2450 6850 2600 6850
Wire Wire Line
	2450 6675 2450 6850
Wire Wire Line
	2150 6950 2600 6950
Wire Wire Line
	2150 7050 2600 7050
Wire Wire Line
	2450 7150 2600 7150
Wire Wire Line
	2450 7150 2450 7300
Wire Wire Line
	3500 6850 3950 6850
Wire Wire Line
	3500 6950 3500 7050
Wire Wire Line
	3500 7000 4100 7000
Connection ~ 3500 7000
Wire Wire Line
	3500 7150 4250 7150
Wire Wire Line
	1650 1100 2350 1100
Wire Wire Line
	2350 1100 2350 1200
Connection ~ 2350 1200
Wire Wire Line
	1650 1400 2350 1400
Wire Wire Line
	2350 1400 2350 1300
Connection ~ 2350 1300
Wire Wire Line
	2300 6800 2300 6950
Wire Wire Line
	1650 6800 2300 6800
Connection ~ 2300 6950
Wire Wire Line
	1650 6850 1650 6800
Wire Wire Line
	2300 7150 2300 7050
Connection ~ 2300 7050
Wire Wire Line
	1650 7150 2300 7150
Wire Wire Line
	2550 1400 2550 1575
Wire Wire Line
	2550 1575 4600 1575
Connection ~ 2550 1400
Wire Wire Line
	2550 2550 2550 2725
Wire Wire Line
	2550 2725 4600 2725
Connection ~ 2550 2550
Wire Wire Line
	2550 3700 2550 3875
Wire Wire Line
	2550 3875 4575 3875
Connection ~ 2550 3700
Wire Wire Line
	2575 4850 2575 5025
Connection ~ 2575 4850
Wire Wire Line
	2550 6000 2550 6150
Wire Wire Line
	2550 6150 4525 6150
Connection ~ 2550 6000
Wire Wire Line
	2550 7150 2550 7325
Wire Wire Line
	2550 7325 4500 7325
Connection ~ 2550 7150
Wire Wire Line
	1325 1000 2450 1000
Connection ~ 2450 1000
Wire Wire Line
	1325 1500 2450 1500
Connection ~ 2450 1500
Wire Wire Line
	2450 2150 1850 2150
Wire Wire Line
	1850 2150 1775 2200
Connection ~ 2450 2150
Wire Wire Line
	1850 2550 1775 2500
Connection ~ 2450 2550
Wire Wire Line
	1775 3375 1825 3325
Wire Wire Line
	1825 3325 2450 3325
Connection ~ 2450 3325
Wire Wire Line
	2450 3750 1825 3750
Wire Wire Line
	1825 3750 1775 3675
Connection ~ 2450 3750
Wire Wire Line
	1750 4525 1800 4450
Wire Wire Line
	1800 4450 2450 4450
Connection ~ 2450 4450
Wire Wire Line
	1750 4825 1800 4900
Wire Wire Line
	1800 4900 2450 4900
Connection ~ 2450 4900
Wire Wire Line
	1750 5675 1800 5625
Wire Wire Line
	1800 5625 2450 5625
Connection ~ 2450 5625
Wire Wire Line
	1750 5975 1800 6050
Wire Wire Line
	1800 6050 2450 6050
Connection ~ 2450 6050
Connection ~ 2450 6725
Wire Wire Line
	1375 6825 1375 6725
Wire Wire Line
	1375 6725 2450 6725
Wire Wire Line
	1375 7125 1375 7250
Wire Wire Line
	1375 7250 2450 7250
Connection ~ 2450 7250
Wire Wire Line
	1325 1000 1325 1100
Wire Wire Line
	1325 1400 1325 1500
Wire Wire Line
	3925 1100 3925 875 
Wire Wire Line
	3925 875  4600 875 
Wire Wire Line
	4050 1250 4050 975 
Wire Wire Line
	4200 1400 4200 1075
Wire Wire Line
	4600 1575 4600 1175
Connection ~ 4600 1575
Connection ~ 4600 1475
Connection ~ 4600 1375
Connection ~ 4600 1275
Wire Wire Line
	3950 2250 3950 1900
Wire Wire Line
	3950 1900 4600 1900
Wire Wire Line
	4100 2400 4100 2000
Wire Wire Line
	4100 2000 4600 2000
Wire Wire Line
	4250 2550 4250 2100
Wire Wire Line
	4250 2100 4600 2100
Wire Wire Line
	4600 2725 4600 2200
Connection ~ 4600 2600
Connection ~ 4600 2500
Wire Wire Line
	4600 2400 4600 2425
Connection ~ 4600 2425
Connection ~ 4600 2300
Wire Wire Line
	3950 3400 3950 2950
Wire Wire Line
	3950 2950 4575 2950
Wire Wire Line
	4250 3700 4250 3150
Wire Wire Line
	4250 3150 4575 3150
Wire Wire Line
	3500 3550 4100 3550
Wire Wire Line
	4100 3550 4100 3050
Wire Wire Line
	4100 3050 4575 3050
Wire Wire Line
	4575 3875 4575 3250
Connection ~ 4575 3350
Connection ~ 4575 3450
Connection ~ 4575 3550
Connection ~ 4575 3650
Wire Wire Line
	3950 4550 3950 4025
Wire Wire Line
	3950 4025 4550 4025
Wire Wire Line
	4100 4700 4100 4125
Wire Wire Line
	4100 4125 4550 4125
Wire Wire Line
	4250 4850 4250 4225
Wire Wire Line
	4250 4225 4550 4225
Connection ~ 4550 4725
Wire Wire Line
	4550 4325 4550 5025
Connection ~ 4550 4625
Connection ~ 4550 4425
Connection ~ 4550 4525
Wire Wire Line
	4550 5025 2575 5025
Wire Wire Line
	3950 5700 3950 5225
Wire Wire Line
	3950 5225 4525 5225
Wire Wire Line
	4525 5325 4075 5325
Wire Wire Line
	4075 5325 4075 5850
Wire Wire Line
	4200 6000 4200 5425
Wire Wire Line
	4200 5425 4525 5425
Wire Wire Line
	4525 6150 4525 5525
Connection ~ 4525 5925
Connection ~ 4525 5825
Connection ~ 4525 5725
Connection ~ 4525 5625
Wire Wire Line
	3950 6850 3950 6500
Wire Wire Line
	3950 6500 4500 6500
Wire Wire Line
	4100 7000 4100 6600
Wire Wire Line
	4100 6600 4500 6600
Wire Wire Line
	4250 7150 4250 6700
Wire Wire Line
	4250 6700 4500 6700
Wire Wire Line
	4500 7325 4500 6800
Connection ~ 4500 7200
Connection ~ 4500 6900
Connection ~ 4500 7100
Connection ~ 4500 7000
Wire Wire Line
	4200 1075 4600 1075
Wire Wire Line
	4050 975  4600 975 
$Comp
L CONN_01X05 J6
U 1 1 59CEFF22
P 4800 1075
F 0 "J6" H 4800 1375 50  0000 C CNN
F 1 "CONN_01X05" V 4900 1075 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4800 1075 50  0001 C CNN
F 3 "" H 4800 1075 50  0001 C CNN
	1    4800 1075
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J7
U 1 1 59CF01A7
P 4800 2100
F 0 "J7" H 4800 2400 50  0000 C CNN
F 1 "CONN_01X05" V 4900 2100 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4800 2100 50  0001 C CNN
F 3 "" H 4800 2100 50  0001 C CNN
	1    4800 2100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J5
U 1 1 59CF0245
P 4775 3150
F 0 "J5" H 4775 3450 50  0000 C CNN
F 1 "CONN_01X05" V 4875 3150 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4775 3150 50  0001 C CNN
F 3 "" H 4775 3150 50  0001 C CNN
	1    4775 3150
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J4
U 1 1 59CF0562
P 4750 4225
F 0 "J4" H 4750 4525 50  0000 C CNN
F 1 "CONN_01X05" V 4850 4225 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4750 4225 50  0001 C CNN
F 3 "" H 4750 4225 50  0001 C CNN
	1    4750 4225
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J3
U 1 1 59CF08BA
P 4725 5425
F 0 "J3" H 4725 5725 50  0000 C CNN
F 1 "CONN_01X05" V 4825 5425 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4725 5425 50  0001 C CNN
F 3 "" H 4725 5425 50  0001 C CNN
	1    4725 5425
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 J2
U 1 1 59CF0980
P 4700 6700
F 0 "J2" H 4700 7000 50  0000 C CNN
F 1 "CONN_01X05" V 4800 6700 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MicroLatch-53253-0570_05x2.00mm_Straight" H 4700 6700 50  0001 C CNN
F 3 "" H 4700 6700 50  0001 C CNN
	1    4700 6700
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR042
U 1 1 59DBE5EB
P 2450 6675
F 0 "#PWR042" H 2450 6525 50  0001 C CNN
F 1 "+3.3V" H 2450 6815 50  0000 C CNN
F 2 "" H 2450 6675 50  0001 C CNN
F 3 "" H 2450 6675 50  0001 C CNN
	1    2450 6675
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR043
U 1 1 59DBE639
P 2450 5550
F 0 "#PWR043" H 2450 5400 50  0001 C CNN
F 1 "+3.3V" H 2450 5690 50  0000 C CNN
F 2 "" H 2450 5550 50  0001 C CNN
F 3 "" H 2450 5550 50  0001 C CNN
	1    2450 5550
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR044
U 1 1 59DBE7C0
P 2450 4400
F 0 "#PWR044" H 2450 4250 50  0001 C CNN
F 1 "+3.3V" H 2450 4540 50  0000 C CNN
F 2 "" H 2450 4400 50  0001 C CNN
F 3 "" H 2450 4400 50  0001 C CNN
	1    2450 4400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR045
U 1 1 59DBE95D
P 2450 3250
F 0 "#PWR045" H 2450 3100 50  0001 C CNN
F 1 "+3.3V" H 2450 3390 50  0000 C CNN
F 2 "" H 2450 3250 50  0001 C CNN
F 3 "" H 2450 3250 50  0001 C CNN
	1    2450 3250
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR046
U 1 1 59DBEB84
P 2450 2100
F 0 "#PWR046" H 2450 1950 50  0001 C CNN
F 1 "+3.3V" H 2450 2240 50  0000 C CNN
F 2 "" H 2450 2100 50  0001 C CNN
F 3 "" H 2450 2100 50  0001 C CNN
	1    2450 2100
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR047
U 1 1 59DBECF7
P 2450 950
F 0 "#PWR047" H 2450 800 50  0001 C CNN
F 1 "+3.3V" H 2450 1090 50  0000 C CNN
F 2 "" H 2450 950 50  0001 C CNN
F 3 "" H 2450 950 50  0001 C CNN
	1    2450 950 
	1    0    0    -1  
$EndComp
Text Label 3600 1100 0    60   ~ 0
RX_00
Text Label 3600 1250 0    60   ~ 0
EN_00
Text Label 3600 1400 0    60   ~ 0
TX_00
Text Label 3600 2250 0    60   ~ 0
RX_01
Text Label 3600 2400 0    60   ~ 0
EN_01
Text Label 3600 2550 0    60   ~ 0
TX_01
Text Label 3600 3400 0    60   ~ 0
RX_02
Text Label 3600 3550 0    60   ~ 0
EN_02
Text Label 3600 3700 0    60   ~ 0
TX_02
Text Label 3600 4550 0    60   ~ 0
RX_03
Text Label 3600 4700 0    60   ~ 0
EN_03
Text Label 3600 4850 0    60   ~ 0
TX_03
Text Label 3600 5700 0    60   ~ 0
RX_04
Text Label 3600 5850 0    60   ~ 0
EN_04
Text Label 3600 6000 0    60   ~ 0
TX_04
Text Label 3600 6850 0    60   ~ 0
RX_05
Text Label 3600 7000 0    60   ~ 0
EN_05
Text Label 3600 7150 0    60   ~ 0
TX_05
$EndSCHEMATC
